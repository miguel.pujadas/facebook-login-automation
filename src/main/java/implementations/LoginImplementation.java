package implementations;
import details.LoginDetails;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginImplementation extends LoginDetails{
    public LoginImplementation(){
        super();
        setUser("test@gmail.com");
        setPassword("123456");
    }
    /*Si no rellenas el campo de usuario o contrasena, por defecto se introduciran estos valores. A veces al realizar pruebas utilizamos valores estaticos, asi que es mas rapido para el tester
    dejas en vacio los parametros que no vaya a necesitar cambiar por defecto*/
    public LoginImplementation(String user, String password){
        super();
        if(user!=null)setUser(user);
        else
            setUser("test@gmail.com");
        if(password!=null)setPassword(password);
        else
            setPassword("123456");
    }
}
