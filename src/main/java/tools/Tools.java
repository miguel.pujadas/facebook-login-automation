package tools;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class Tools {
    //Esta funcion la he creado para generar esperas implicitas a la hora de buscar elementos
    public void waitToElement(WebDriver driver, By element, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
}
