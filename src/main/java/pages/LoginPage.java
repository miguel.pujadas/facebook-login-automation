package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import details.LoginDetails;
import factories.LoginFactory;
import tools.Tools;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class LoginPage {
    private static LoginDetails pageFactory;
    public LoginPage(WebDriver driver){
        this.pageFactory = new LoginDetails(driver);
    }

    public static void login(WebDriver driver, String user, String password){
        Tools tools = new Tools();
        LoginDetails loginDetails = LoginFactory.createLoginData(user, password);
        //He creado la función waitToElement para poder generar esperas implícitas, y asi asegurarme que el driver llega al elemento aunque la pagina por algun motivo tarde demasiado en cargar
        tools.waitToElement(driver, By.id( pageFactory.getUserInput().getAttribute("id")),5);
        //Relleno el campo de usuario
        pageFactory.getUserInput().sendKeys(loginDetails.getUser());
        //Relleno el campo de contrasena
        pageFactory.getUserPasswordInput().sendKeys(loginDetails.getPassword());
        tools.waitToElement(driver, By.id( pageFactory.getLoginButton().getAttribute("id")),8);
        //Hago click en el boton de login
        pageFactory.getLoginButton().click();
    }
}
