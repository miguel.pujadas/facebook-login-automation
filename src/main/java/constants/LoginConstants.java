package constants;

/**
 * Created by Miguel Pujadas Palenzuela
 */

public class LoginConstants {
    public static final String USER_ID = "email";
    public static final String PASSWORD_ID = "pass";
    public static final String LOGIN_BUTTON_XPATH= "//*[starts-with(@id, 'u_0_')]";
}
