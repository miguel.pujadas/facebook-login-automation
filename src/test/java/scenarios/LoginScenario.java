package scenarios;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class LoginScenario {
    public void login(WebDriver driver, String user, String password) throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        driver.get("https://www.facebook.com/");
        loginPage.login(driver,user,password);
    }
}
