import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import scenarios.LoginScenario;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class LoginTest extends LoginScenario {
    public static final String user = System.getProperty("user");
    public static final String password = System.getProperty("password");
    private static WebDriver driver;
    @BeforeMethod(alwaysRun = true)
    public void setup() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        driver = new ChromeDriver(capability);
        driver.manage().window().maximize();
    }
    @Test
    public void circuit() throws Exception {
        // You can send a custom user and password by Maven: mvn test -DLoginTest -Duser=”user” -Dpassword=”password”
        login(driver,user,password);
    }
    @AfterMethod(alwaysRun = true)
    public void teardown() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Hello World");
        driver.quit();
    }
}
